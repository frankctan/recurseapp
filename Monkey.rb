class Array
  def sum
    s = 0
    self.each do |x|
      s += x
    end
    return s
  end

  def to_i
    self.map{|x| x.to_i}
  end
end

class String
  def to_boolean
    self.downcase == 'true'
  end
end

class Fixnum
  def to_a
    [self]
  end
end