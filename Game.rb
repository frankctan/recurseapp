
class Game
  load 'Player.rb'
  load 'Monkey.rb'

  attr_accessor :size, :board, :player_board, :player, :win_condition

  def initialize(player_num = 2)
    @player = Array.new
    @win_condition = Array.new
    @size = 0
    raise "Player Number Error" if player_num < 2
    marker = ('A'..'Z').to_a
    #initializes players
    0.upto(player_num-1) do |x|
      @player[x] = Player.new(marker[x])
    end

    game_controller

  end

  def game_controller
    game_flag = true

    while game_flag do
      puts "Board Size?"
      @size = gets.chomp.to_i
      initialize_board
      win_conditions
      refresh
      play
      puts "Play Again? (true/false)"
      game_flag = gets.chomp.to_boolean
    end

    show_record
  end

  #Think about game flow!
  def play
    while(1) do 
      @player.each_index do |x|

        puts "\nPlayer #{x+1}'s move"

        wrong_move_flag = true
        3.times do
          turn = gets.chomp.to_i
          #validity check
          if @player_board[turn] == "  " && turn < @board.length
            wrong_move_flag = false
            @player[x].move(turn)
            @player_board[turn] = @player[x].marker + " "
            @board[turn] = "  "
            break
          else
            refresh
            puts "\nInvalid play. Player #{x+1}'s move"
          end
        end

        if wrong_move_flag
          refresh
          puts "\n Too many wrong moves! Player #{x+1} loses!"
          player[x].loss
          player.each {|p| p.win unless p == player[x]}
          return
        end

        refresh
        
        if check_win(player[x])
          puts "\nPlayer #{x+1} Wins!"
          player[x].win #Assign wins and losses!
          player.each {|p| p.loss unless p == player[x]}
          return
        end

        if check_tie
          puts "Tie Game!"
          player.each {|p| p.tie}
          return
        end
      
      end
    end
  end

  def check_win(player)
    @win_condition.each do |combo|
      return true if combo & player.history.map{|x| x.to_i} == combo
    end
    false
  end

  def check_tie
    return true if @board.map{|x| x.to_i}.sum == 0
    false
  end

  #reset player history, boards and win conditions
  def initialize_board
    player.each {|p| p.history = []}
    @counter = -1
    @board = Array.new(@size * @size).map{|x| counter}
    @player_board = Array.new(@size * @size, "  ")
    win_conditions
  end

  #determines all win conditions on the board
  def win_conditions
    @win_condition = [] #initialize for new game
    (0..@board.length-1).each do |x|
      if x % @size < @size - 2 #if x-index is at most 2 from right
        @win_condition << [x, x+1, x+2] #horizontal win
        if x < @size * (@size - 2) #if y-index is at most 2 from bottom
          @win_condition << [x, x+@size+1, x+@size*2+2] #diagonal win
        end
      end
      if x < @size * (@size - 2) #same as above
        @win_condition << [x, x+@size, x+@size*2] #vertical win
      end

    end
  end

  #used in initialize board only
  def counter
    @counter = @counter + 1
    sprintf '%02d', @counter #displays using 2 characters
  end

  def refresh
    system "clear"
    show_record
    draw_board
  end

  #redraw board
  def draw_board
    @player_board.each_slice(@size){ |x| p x}
    puts ""
    @board.each_slice(@size){ |x| p x}
    puts ""
  end

  def show_record
    player.each_index{|p| puts "Player #{p+1}'s Record: #{player[p].record}"}
    puts ""
  end

end



