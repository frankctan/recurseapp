class Player
  attr_accessor :history, :record, :marker

  def initialize(marker)
    @marker = marker
    @history = Array.new
    @record = {win: 0, loss: 0, tie: 0}
  end


  def move(player_move)
    @history << player_move
  end

  def invalid_move
    puts "Invalid move. Input valid move: "
    @history -= Array.new(1,@history.last)
    move
  end

  def win
    @record[:win] += 1
  end

  def loss
    @record[:loss] += 1
  end

  def tie
    @record[:tie] += 1
  end

end

